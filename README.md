viSar bruteforce
=============

## A tool to brute force admin password of viSar IP DVR

List of passwords is read from stdin.

Usage:

`cat dictionary.txt | ruby bruteforce.rb ip port user`

Example:

`zcat weak_passwords.txt.gz | ruby bruteforce.rb 192.168.1.149 6036 admin`

or

`hashcat.bin --stdout -a 3 ?d?d?d?d | ruby bruteforce.rb 192.168.1.149 6036 user`
